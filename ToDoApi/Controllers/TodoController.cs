﻿namespace ToDoApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.AspNetCore.Mvc;

    using ToDoApi.Context;
    using ToDoApi.Models;

    [Route("api/[controller]")]
    public class TodoController : Controller
    {
        private readonly ToDoContext context;

        public TodoController(ToDoContext todoContext)
        {
            this.context = todoContext;

            if (this.context.TodoItems.Any())
            {
                return;
            }

            this.context.TodoItems.Add(new ToDoItem() { Name = "Item1" });
            this.context.SaveChanges();
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<ToDoItem> Get()
        {
            return this.context.TodoItems.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var item = this.context.TodoItems.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return this.NotFound();
            }

            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]ToDoItem item)
        {
            if (item == null)
            {
                return this.BadRequest();
            }

            this.context.TodoItems.Add(item);
            this.context.SaveChanges();

            return this.CreatedAtRoute("Get", new { id = item.Id }, item);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]ToDoItem item)
        {
            if (item == null || item.Id != id)
            {
                return this.BadRequest();
            }

            var todo = this.context.TodoItems.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return this.NotFound();
            }

            todo.IsComplete = item.IsComplete;
            todo.Name = item.Name;

            this.context.TodoItems.Update(todo);
            this.context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var todo = this.context.TodoItems.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return this.NotFound();
            }

            this.context.TodoItems.Remove(todo);
            this.context.SaveChanges();

            return new NoContentResult();
        }
    }
}
